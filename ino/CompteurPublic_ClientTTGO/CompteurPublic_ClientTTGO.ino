/*
 * 
 * Select the development board ESP32 Dev Module, select Disable in the PSRAM option, select 4MB in the Flash Size option
 * 
 */

#include "WiFi.h"
#include <esp_now.h>

//DISPLAY
#include <SPI.h>
#include <TFT_eSPI.h>       // Hardware-specific library

TFT_eSPI tft = TFT_eSPI();  // Invoke custom library


///////////////////////////////////////////////////////////////////////////////
//ESP-NOW related
///////////////////////////////////////////////////////////////////////////////

String success;

// Replace with the MAC Address of the master
uint8_t master[] = {0xXX, 0xXX, 0xXX, 0xXX, 0xXX, 0xXX};

// Define variables to store incoming readings
int incomingT;
int lastIncomingT;
int c;
int lastSent = 0;
int delta = 5000;

// PINS OF SWITCHES
#define btnUp   27
#define btnDown 26
boolean UpSwitchReleased;
boolean DownSwitchReleased;

//Connection check
long lastUpdateReceived;
int deltaUpdate= 2000;
boolean stateConnected;
boolean lastStateConnected;


/////////////////////////////////////////////////////////////////////
//ESP-NOW related
/////////////////////////////////////////////////////////////////////

//Structure example to send data
//Must match the receiver structure
typedef struct struct_message {
  int c; // quel dumbphone vient de m'envoyer un message
} struct_message;

// Create a struct_message to hold sended coordinates
struct_message myCount;

// Callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
  if (status == 0) {
    success = "Delivery Success :)";
    c = 0;
  }
  else {
    success = "Delivery Fail :(";
  }
}

// Callback when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  //memcpy(&receivedCoordinates, incomingData, sizeof(receivedCoordinates));
  // essai avec memmove pour éviter les débordemments
  memmove(&myCount, incomingData, sizeof(myCount));
  incomingT = myCount.c;
  lastUpdateReceived = millis();
  //updateDisplay();
}

void sendToMaster()
{
  //Send my text to other devices
  struct_message myCount;
  myCount.c = c;
  esp_err_t result = esp_now_send(master, (uint8_t *) &myCount, sizeof(myCount));
  if (result == ESP_OK) {
    Serial.println("Sent with success");
  }
  else {
    Serial.println("Error sending the data");
  }
}

esp_now_peer_info_t peerInfo;

void setup() {
  
  tft.init();
  tft.setRotation(1);
  tft.fillScreen(TFT_BLACK);
  

  //INPUT
  pinMode(btnUp, INPUT_PULLUP);
  pinMode(btnDown, INPUT_PULLUP);

 Serial.begin(115200);

  //////////////////////////////////////////////////////////////
  // ESP-NOW

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);

  // Register peer
  
  memcpy(peerInfo.peer_addr, master, 6);
  peerInfo.channel = 0;
  peerInfo.encrypt = false;

   // Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }

  // Register for a callback function that will be called when data is received
  esp_now_register_recv_cb(OnDataRecv);


}

void loop()
{
  //////////////////////////////////////////////////////
  ////////           BUTTONS                   /////////
  //////////////////////////////////////////////////////

  if (digitalRead(btnUp) == 1 && UpSwitchReleased == true)
  {
    Serial.println("UP");
    UpSwitchReleased = false;
    c++;
    updateDisplay();
    //delay(10);
  }

  else if (digitalRead(btnUp) == 0 && UpSwitchReleased == false)
  {
    UpSwitchReleased = true;
    //delay(10);
  }
  
  if (digitalRead(btnDown) == 0 && DownSwitchReleased == true)
  {
    Serial.println("DOWN");
    DownSwitchReleased = false;
    c--;
    updateDisplay();
  } 
  
  else if (digitalRead(btnDown) == 1 && DownSwitchReleased == false)
  {
    DownSwitchReleased = true;
  }

  if (millis()>lastSent+delta)
  {
    sendToMaster();
    
    lastSent = millis();
  }

  // check connection state

  if (millis()>lastUpdateReceived+ deltaUpdate)
  {
    stateConnected = false;
  } else{
    stateConnected = true;
  }

  // and update screen when state has change

  if (stateConnected == true && lastStateConnected == false)
  {
    updateDisplay();
    lastStateConnected = true;
  } else if (stateConnected == false && lastStateConnected == true)
  {
    updateDisplay();
    lastStateConnected = false;
  }


  if (incomingT != lastIncomingT)
  {
    lastIncomingT = incomingT;
    updateDisplay();
  }
  
  delay(10);
}

//////////////////////////////////////////////////////
////////           DISPLAY                   /////////
//////////////////////////////////////////////////////

void updateDisplay()
{
  tft.fillScreen(TFT_BLACK);
  tft.setTextColor(TFT_RED, TFT_BLACK);
  tft.setCursor(10, 20, 4);
  tft.print("Compteur: ");
  tft.print(c);
  tft.setCursor(10, 40, 4);
  tft.print("---");
  tft.setCursor(10, 60, 4);
  tft.setTextColor(TFT_GREEN, TFT_BLACK);
  tft.print("Total sur site: ");
  tft.print(incomingT);
  tft.setTextColor(TFT_RED, TFT_BLACK);
  tft.setCursor(10, 80, 4);
  tft.print("---");
  tft.setTextColor(TFT_WHITE, TFT_BLACK);
  tft.setCursor(10, 100, 4);
  if (stateConnected) tft.print("*");

}
