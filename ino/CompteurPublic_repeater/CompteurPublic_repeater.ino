/*Si problème MD5 Flash:  esptool.py --port /dev/ttyUSB0 write_flash_status --non-volatile 0

   COUNTER Select the development board ESP32 Dev Module, select Disable in the PSRAM option, select 4MB in the Flash Size option
   Upload via ESP32 data upload tool

*/
// Montage JSON
/* Role 1: COUNTER, Role 2: REPEATER, Role 3: MASTER */

#include "FS.h"
#include "SPIFFS.h"
#include <ArduinoJson.h>
#include "WiFi.h"
#include <esp_now.h>

// Set TRUE to format SPIFFS
#define FORMAT_SPIFFS_IF_FAILED false

// Max nbr of devices handled by the entire system
#define NBR_DEVICES 20

// JSON  where we store MACS and Roles. Upload via ESP32 data upload tool
String fileName = "config.json";
DynamicJsonDocument doc(1800);
JsonArray arr; // array extracted from json
int count; //array size

////////////////////////////////////////////////
// MAC Storage

// Array to store everyBodys MAC.
uint8_t MAC_arrays[NBR_DEVICES][6];

// MAC Address of master (only 1)
uint8_t MAC_master[6];

// Array to store repeaters MAC.
uint8_t MAC_RepeatersArray[NBR_DEVICES][6];
int nbrOfRepeaters = 0;

// Array to store Counters MAC.
uint8_t MAC_CountersArray[NBR_DEVICES][6];
int nbrOfCounters = 0;

//ME
uint8_t MAC_array[6];
int my_device = -1;
int role = 0;

////////////////////////////////////////////////

// Récuparation de la liste des adresses MAC dans un fichier Json stocké dans la SPIFF
void listDir(fs::FS &fs, const char * dirname, uint8_t levels)
{
  Serial.println();
  Serial.printf("Listing directory: %s\r\n", dirname);

  File root = fs.open(dirname);
  // On parcours les dossiers et les fichiers à l'intérieur
  if (!root)
  {
    Serial.println("- failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println(" - not a directory");
    return;
  }

  File file = root.openNextFile();

  while (file)
  {
    if (file.isDirectory())
    {
      Serial.print("  DIR : ");
      Serial.println(file.name());
      if (levels)
      {
        listDir(fs, file.name(), levels - 1);
      }
    }
    else
    {
      Serial.print("FILE: ");
      Serial.print(file.name());
      Serial.print("\tSIZE: ");
      Serial.println(file.size());

      String str1 = String(file.name()); //convert char to String
      //
      // On cherche un fichier précis, config.json
      if (str1 == fileName)
      {
        Serial.println("Found config.json");

        auto error = deserializeJson(doc, file);
        if (error) {
          Serial.println("Failed to parse the file");
          // return false;
        }
        else
        {
          Serial.println("Success to parse the file");
          // Get a reference to the root array
          arr = doc.as<JsonArray>();

          // Get the number of elements in the array
          count = arr.size();
          int iterator = 0;

          // Walk the JsonArray efficiently
          for (JsonObject repo : arr)
          {
            Serial.println();
            Serial.println();
            Serial.printf("Device n°: %i\r\n", iterator);
            Serial.println("---");
            Serial.print("Role: ");
            int temp_role = repo["role"];
            Serial.println(temp_role);

            for (int i = 0; i < 6; i++)

              const char* temp_mac = repo["mac"][i];
            char temp[5]; // Temporary space for the conversion string
            // Copy 4 characters into the temporary string
            temp[0] = temp_mac[0];
            temp[1] = temp_mac[1];
            temp[2] = temp_mac[2];
            temp[3] = temp_mac[3];
            // and terminate the string properly
            temp[4] = 0;
            // Convert the string using base 16
            uint8_t val = strtol(temp, NULL, 16);
            MAC_arrays[iterator][i] = val;
            Serial.print(val);
            Serial.print(":");

            // Storing it into the right Array
            /* Role 1: COUNTER, Role 2: REPEATER, Role 3: MASTER */
            switch (temp_role)
            {
              //COUNTER
              case 1:
                MAC_CountersArray[nbrOfCounters][i] = val;
                break;

              //REPEATER
              case 2:
                MAC_RepeatersArray[nbrOfRepeaters][i] = val;
                break;

              //MASTER
              case 3:
                MAC_master[i] = val;
                break;
            }

          }
          switch (temp_role)
          {
            //COUNTER
            case 1:
              nbrOfCounters++;
              break;

            //REPEATER
            case 2:
              nbrOfRepeaters++;
              break;

          }

          iterator++;
        }
        Serial.println();
        Serial.println("---");

        return;
      }

    }
  }
  file = root.openNextFile();
}

}



void setup() {

  Serial.begin(115200);

  if (!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED)) {
    Serial.println("SPIFFS Mount Failed");
    return;
  }
  listDir(SPIFFS, "/", 0);

  /////////////////////////////////
  /// IDENTIFICATION OF DEVICES ///
  /////////////////////////////////

  //MY MAC
  Serial.println();
  Serial.print("My Mac is: ");
  WiFi.macAddress(MAC_array);
  for (int i = 0; i < sizeof(MAC_array); ++i) {
    //sprintf(MAC_char,"%s%02x:",MAC_char,MAC_array[i]);
    Serial.print(MAC_array[i]);
    Serial.print(":");
  }

  Serial.println();
  Serial.println();

  //COMPARAISON
  for (int m = 0; m < NBR_DEVICES; m++)
  {
    int score = 0;
    for (int n = 0; n < 6; n++)
    {
      if (MAC_array[n] == MAC_arrays[m][n])score++;
    }
    if (score == 6) my_device = m;
  }
  if (my_device != -1)
  {
    Serial.print("My Device is n° ");
    Serial.print(my_device);
    Serial.println("inside Json");
  } else
  {
    Serial.println("I don't know who I am !!!!");
  }

  //////////////////////////////////////
  /// OTHER DEVICES ARE YOUR FRIENDS ///
  //////////////////////////////////////
  Serial.println();
  Serial.println();
  Serial.println("////////////////////////////////");
  Serial.println();

  Serial.println();
  Serial.println("Master is: ");
  for (int i = 0; i < 6; i++)
  {
    Serial.print(MAC_master[i]);
    Serial.print(":");
  }
  Serial.println();
  Serial.println();
  Serial.println("////////////////////////////////");
  Serial.println();

  Serial.println("Repeaters are: ");
  Serial.println("--");
  for (int j = 0; j < nbrOfRepeaters; j++)
  {
    Serial.printf("Device n°: %i\r\n", j);
    for (int i = 0; i < 6; i++)
    {
      Serial.print(MAC_RepeatersArray[j][i]);
      Serial.print(":");
    }
    Serial.println();
  }

  Serial.println();
  Serial.println("////////////////////////////////");
  Serial.println();

  Serial.println("Counters are: ");
  Serial.println("--");
  for (int j = 0; j < nbrOfCounters; j++)
  {
    Serial.printf("Device n°: %i\r\n", j);

    for (int i = 0; i < 6; i++)
    {
      Serial.print(MAC_CountersArray[j][i]);
      Serial.print(":");
    }
    Serial.println();
    Serial.println();
  }
  Serial.println();
  Serial.println();

}

void loop() {
  // put your main code here, to run repeatedly:

}
