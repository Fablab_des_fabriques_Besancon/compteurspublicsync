#include "WiFi.h"
#include <esp_now.h>
#include <M5Stack.h>
#include "params.h"
#include <Preferences.h>

Preferences preferences;

String success;

#define nbrOfDevices 3 //(needs to be updated in params.h too)

// Define variables to store incoming readings
int incomingC;
int total;
int lastSent = 0;
int delta = 1000;

/////////////////////////////////////////////////////////////////////
//ESP-NOW related
/////////////////////////////////////////////////////////////////////

//Structure example to send data
//Must match the receiver structure
typedef struct struct_message {
  int c;
} struct_message;

// Create a struct_message to hold sended coordinates
struct_message myCount;

// Callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
  if (status == 0) {
    success = "Delivery Success :)";
  }
  else {
    success = "Delivery Fail :(";
  }
}

// Callback when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  //memcpy(&receivedCoordinates, incomingData, sizeof(receivedCoordinates));
  // essai avec memmove pour éviter les débordemments
  memmove(&myCount, incomingData, sizeof(myCount));
  incomingC = myCount.c;
  total += incomingC;
  Serial.println("receive: ");
}

void sendToOthers()
{
  //Send my text to other devices
  struct_message myTotal;
  myTotal.c = total;
  for (int m = 0; m < nbrOfDevices; m++)
  {
    esp_err_t result = esp_now_send(broadcastAddresses[m], (uint8_t *) &myTotal, sizeof(myTotal));
    if (result == ESP_OK) {
      Serial.print("Sent with success to MAC ");
      Serial.println(m);

    }
    else {
      Serial.print("Error sending the data to MAC ");
      Serial.println(m);
    }
  }
}

esp_now_peer_info_t peerInfo;

void setup() {
  M5.begin();
  M5.Lcd.fillScreen(BLACK);

  M5.Lcd.setTextColor(GREEN);
  Serial.begin(115200);

  //////////////////////////////////////////////////////////////
  // ESP-NOW

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);

  // Register peers

  peerInfo.channel = 0;
  peerInfo.encrypt = false;

  for (int m = 0; m < nbrOfDevices; m++)
  {
    memcpy(peerInfo.peer_addr, broadcastAddresses[m], 6);
    // Add peer
    if (esp_now_add_peer(&peerInfo) != ESP_OK) {
      Serial.println("Failed to add peer");
      return;
    }
  }

  // Register for a callback function that will be called when data is received
  esp_now_register_recv_cb(OnDataRecv);

  // The master remembers his last total when shutdown or reboot
  preferences.begin("my-app", false);
  int storedTotal = preferences.getUInt("total", 0);
  total = storedTotal;

}

void loop()
{
  
  // Update screen
  if (millis() > lastSent + delta)
  {
    sendToOthers();
    lastSent = millis();
    M5.Lcd.fillScreen(BLACK);
    M5.Lcd.setCursor(10, 100);
    M5.Lcd.setTextColor(WHITE);
    M5.Lcd.setTextSize(3);
    M5.Lcd.printf("Sur site: ");
    M5.Lcd.print(total);
    M5.Lcd.setCursor(10, 150);
    M5.Lcd.setTextColor(YELLOW);
    M5.Lcd.setTextSize(2);
    M5.Lcd.println("Bouton 1 pendant 1s");
    M5.Lcd.setCursor(10, 170);
    M5.Lcd.println("=> remise a 0");
    backupTotal();
  }

  // Listen to button > to reset zero
  M5.update();

  if (M5.BtnA.wasReleasefor(1000)) {
    M5.Lcd.clear(BLACK);  // Clear the screen and set white to the background color.
    total = 0;
    preferences.putUInt("total", 0);
  }

}


void backupTotal()
{
  preferences.putUInt("total", total);
}
