/*Si problème MD5 Flash:  esptool.py --port /dev/ttyUSB0 write_flash_status --non-volatile 0

   COUNTER Select the development board ESP32 Dev Module, select Disable in the PSRAM option, select 4MB in the Flash Size option
   Upload via ESP32 data upload tool

*/
// Montage JSON
/* Role 1: COUNTER, Role 2: REPEATER, Role 3: MASTER */

#include "FS.h"
#include "SPIFFS.h"
#include <ArduinoJson.h>

#include "WiFi.h"
#include <esp_now.h>

#define COUNTER
//#define REPEATER
//#define MASTER

////////////////////////////////////////////////////////

#ifdef COUNTER

//DISPLAY
#include <SPI.h>
#include <TFT_eSPI.h>       // Hardware-specific library

#endif

// Set TRUE to format SPIFFS
#define FORMAT_SPIFFS_IF_FAILED false

// Max nbr of devices handled by the entire system
#define NBR_DEVICES 20

// JSON  where we store MACS and Roles. Upload via ESP32 data upload tool
// The weight of Json needs to be estimated on the JSon library site
String fileName = "config.json";
DynamicJsonDocument doc(1800);
JsonArray arr; // array extracted from json
int count; //array size

////////////////////////////////////////////////
// MAC Storage

// Array to store everyBodys MAC.
uint8_t MAC_arrays[NBR_DEVICES][6];

// MAC Address of master (only 1)
uint8_t MAC_master[6];

// Array to store repeaters MAC.
uint8_t MAC_RepeatersArray[NBR_DEVICES][6];
int nbrOfRepeaters = 0;

// Array to store Counters MAC.
uint8_t MAC_CountersArray[NBR_DEVICES][6];
int nbrOfCounters = 0;

//ME
uint8_t MAC_array[6];
int role = 0;

////////////////////////////////////////////////

#ifdef COUNTER

TFT_eSPI tft = TFT_eSPI();  // Invoke custom library

//////////////////////////////////////////////////////////////////////////////
//ESP-NOW related
///////////////////////////////////////////////////////////////////////////////

String success;

//DISPLAYED VARS
int in; // how many IN
int out; // how many OUT
int total; // not used when sending, set to -1

// SENDED VARS
int s_node = -1;
int s_in; // how many IN
int s_out; // how many OUT
int s_total; // not used when sending, set to -1

long lastSent = 0;
long lastPingSent;
long lastUpdate;

int delta = 10000; // min time between 2 messages, but we have to wait the confirmation of the master
boolean hasBeenReceived = true;

// Define variables to store incoming readings
int r_node;
int r_in;
int r_out;
int r_total;

// PINS OF SWITCHES
#define btnUp   27
#define btnDown 26

long lastPressUp;
long lastPressDown;
boolean upState = false;
boolean downState = false;

//Connection check
long lastNewsFromMaster;
long lastCheck;
int deltaCheck = 5000;
boolean masterConnected = false;

boolean repeatersConnected[20];
int howManyRepeatersConnected;
int repeaterAvailable;

int retry = 0;



/////////////////////////////////////////////////////////////////////
//ESP-NOW related
/////////////////////////////////////////////////////////////////////

//Structure example to send data
//Must match the receiver structure
typedef struct struct_message {
  int node; // which node has sent the message (in json order)
  int in; //how many IN
  int out; // how many OUT
  int total; //total in da place
  String tree; // we store the path followed by the message
} struct_message;

// Create a struct_message to hold sended coordinates
struct_message myCount;

// Callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {

  uint8_t MAC_callBack[6] = {mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]};

  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");

  // On regarde de qui vient le paquet, et on en déduit qu'il est vivant

  if (status == 0) {
    success = "Delivery Success :)";

    if (compareMACS(MAC_callBack, MAC_master))
    {
      Serial.println("CallBackFromMaster");
      masterConnected = true;
    } else {

      for (int i = 0; i < nbrOfRepeaters; i++)
      {
        if (compareMACS(MAC_callBack, MAC_RepeatersArray[i]))
        {
          Serial.println("CallBackFromRepeater");
          repeatersConnected[i] = true;
          repeaterAvailable = i;
        }
      }
      howManyRepeatersConnected = 0;
      for (int j = 0; j < 20; j++)
      {
        if (repeatersConnected[j] == true) howManyRepeatersConnected++;
      }
      //updateDisplay();
    }

    // Si master rate, on essaie les repeteurs dans l'ordre
  }
  else {
    success = "Delivery Fail :(";

    // L'envoi des datas a échoué, on retry:
    if (hasBeenReceived == false )
    {
      // On repasse has been received à true
      hasBeenReceived = true;
      retry++;
      lastSent = millis();
    }


    //Plus de master
    if (compareMACS(MAC_callBack, MAC_master))
    {
      Serial.println("Master HS");
      masterConnected = false;
    }

  }


}

// Callback when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {

  //memcpy(&receivedCoordinates, incomingData, sizeof(receivedCoordinates));
  // essai avec memmove pour éviter les débordemments
  memmove(&myCount, incomingData, sizeof(myCount));

  r_node = myCount.node;
  r_in = myCount.in;
  r_out = myCount.out;
  r_total = myCount.total;

  // on checke si le paquet est bien pour nous
  if (r_node == s_node)
  {
    // Puisque nos valeurs envoyées ont été prise en compte dans le calcul, on les retire de notre compteur.
    in -= r_in;
    out -= r_out;
    total = r_total;

    // Et on note que l'on a bien reçu une réponse du master
    //lastUpdateReceived = millis();
    hasBeenReceived = true;
  }
  //updateDisplay();
}

boolean compareMACS(uint8_t _mac1[6], uint8_t _mac2[6])
{
  boolean myResult = false;
  int score = 0;
  for (int n = 0; n < 6; n++)
  {
    if (_mac1[n] == _mac2[n])score++;
  }
  if (score == 6)
  {
    return true;
  } else {
    return false;
  }
}

void checkConnections()
{
  // check if Master is available
  struct_message myTest;

  myTest.in = -1;
  myTest.out = -1;
  myTest.total = -1;
  myTest.node = -1;
  myTest.tree = "";

  //PING MASTER
  esp_err_t result = esp_now_send(MAC_master, (uint8_t *) &myTest, sizeof(myTest));

  //CHECK IF REPEATERS ARE AVAILABLE
  //RAZ avant de compter
  howManyRepeatersConnected = 0;
  for (int i = 0; i < 20; i++)
  {
    repeatersConnected[i] = false;
  }

  Serial.printf("on envoie à %i repeteurs un ping", nbrOfRepeaters);
  Serial.println();

  int i = nbrOfRepeaters - 1;
  while (i >= 0)
  {
    if (millis() > lastPingSent + 200)
    {
      Serial.print("Sending PING to Repeater ");
      Serial.println(i);
      esp_now_send(MAC_RepeatersArray[i], (uint8_t *) &myTest, sizeof(myTest));
      lastPingSent = millis();
      i--;
    }
  }

  /*

    for (int i =0; i<nbrOfRepeaters; i++)
    {
    //Serial.println(MAC_RepeatersArray[i][0]);
    esp_now_send(MAC_RepeatersArray[i], (uint8_t *) &myTest, sizeof(myTest));
    delay(100);
    }
  */
}


void sendData()
{
  Serial.println("Send data");
  //Send my text to other devices
  struct_message myCount;

  s_in = in;
  s_out = out;

  myCount.in = s_in;
  myCount.out = s_out;
  myCount.total = -1;
  myCount.node = s_node;
  myCount.tree = "";

  esp_err_t result;
  Serial.println("howManyRepeatersConnected");

  // On envoie directement au Master s'il est dispo
  if (masterConnected)
  {
    result = esp_now_send(MAC_master, (uint8_t *) &myCount, sizeof(myCount));

  } // Sinon ça part sur un répéteur proche
  else if (howManyRepeatersConnected > 0)
  {

    result = esp_now_send(MAC_RepeatersArray[repeaterAvailable], (uint8_t *) &myCount, sizeof(myCount));
  } else {
    //On retry plus tard
    if (hasBeenReceived == false )
    {
      // On repasse has been received à true
      hasBeenReceived = true;
      lastSent = millis();
    }
  }

  if (result == ESP_OK) {
    Serial.println("Sent with success");
  }
  else
  {
    Serial.println("Error sending the data");
  }
}

esp_now_peer_info_t peerInfo;

#endif

// Récuparation de la liste des adresses MAC dans un fichier Json stocké dans la SPIFF
void listDir(fs::FS &fs, const char * dirname, uint8_t levels)
{
  Serial.println();
  Serial.printf("Listing directory: %s\r\n", dirname);

  File root = fs.open(dirname);
  // On parcours les dossiers et les fichiers à l'intérieur
  if (!root)
  {
    Serial.println("- failed to open directory");
    return;
  }
  if (!root.isDirectory())
  {
    Serial.println(" - not a directory");
    return;
  }

  File file = root.openNextFile();

  while (file)
  {
    if (file.isDirectory())
    {
      Serial.print("  DIR : ");
      Serial.println(file.name());
      if (levels)
      {
        listDir(fs, file.name(), levels - 1);
      }
    }
    else
    {
      Serial.print("FILE: ");
      Serial.print(file.name());
      Serial.print("\tSIZE: ");
      Serial.println(file.size());

      String str1 = String(file.name()); //convert char to String
      //
      // On cherche un fichier précis, config.json
      if (str1 == fileName)
      {
        Serial.println("Found config.json");

        auto error = deserializeJson(doc, file);
        if (error) {
          Serial.println("Failed to parse the file");
          // return false;
        }
        else
        {
          Serial.println("Success to parse the file");
          // Get a reference to the root array
          arr = doc.as<JsonArray>();

          // Get the number of elements in the array
          count = arr.size();
          int iterator = 0;

          // Walk the JsonArray efficiently
          for (JsonObject repo : arr)
          {
            Serial.println();
            Serial.println();
            Serial.printf("Device n°: %i\r\n", iterator);
            Serial.println("---");
            Serial.print("Role: ");
            int temp_role = repo["role"];
            Serial.println(temp_role);

            for (int i = 0; i < 6; i++)
            {
              const char* temp_mac = repo["mac"][i];
              char temp[5]; // Temporary space for the conversion string
              // Copy 4 characters into the temporary string
              temp[0] = temp_mac[0];
              temp[1] = temp_mac[1];
              temp[2] = temp_mac[2];
              temp[3] = temp_mac[3];
              // and terminate the string properly
              temp[4] = 0;
              // Convert the string using base 16
              uint8_t val = strtol(temp, NULL, 16);
              MAC_arrays[iterator][i] = val;
              Serial.print(val);
              Serial.print(":");

              // Storing it into the right Array
              /* Role 1: COUNTER, Role 2: REPEATER, Role 3: MASTER */
              switch (temp_role)
              {
                //COUNTER
                case 1:
                  MAC_CountersArray[nbrOfCounters][i] = val;
                  break;

                //REPEATER
                case 2:
                  MAC_RepeatersArray[nbrOfRepeaters][i] = val;
                  break;

                //MASTER
                case 3:
                  MAC_master[i] = val;
                  break;
              }

            }
            switch (temp_role)
            {
              //COUNTER
              case 1:
                nbrOfCounters++;
                break;

              //REPEATER
              case 2:
                nbrOfRepeaters++;
                break;

            }

            iterator++;
          }
          Serial.println();
          Serial.println("---");

          return;
        }

      }
    }
    file = root.openNextFile();
  }
}

void fonction_ISR_UP() {
  if (upState == false && millis() > lastPressUp + 150)
  {
    in++;
    lastPressUp = millis();
  }
  upState = !upState;
}

void fonction_ISR_DOWN() {
  if (downState == false && millis() > lastPressDown + 150)
  {
    out++;
    lastPressDown = millis();
  }
  downState = !downState;
}

void setup()
{

  Serial.begin(115200);

  if (!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED))
  {
    Serial.println("SPIFFS Mount Failed");
    return;
  }
  listDir(SPIFFS, "/", 0);

  /////////////////////////////////
  /// IDENTIFICATION OF DEVICES ///
  /////////////////////////////////

  //MY MAC
  Serial.println();
  Serial.print("My Mac is: ");
  WiFi.macAddress(MAC_array);
  for (int i = 0; i < sizeof(MAC_array); ++i)
  {
    //sprintf(MAC_char,"%s%02x:",MAC_char,MAC_array[i]);
    Serial.print(MAC_array[i]);
    Serial.print(":");
  }

  Serial.println();
  Serial.println();

  //COMPARAISON
  for (int m = 0; m < NBR_DEVICES; m++)
  {
    int score = 0;
    for (int n = 0; n < 6; n++)
    {
      if (MAC_array[n] == MAC_arrays[m][n])score++;
    }
    if (score == 6) s_node = m;
  }
  if (s_node != -1)
  {
    Serial.print("My Device is n° ");
    Serial.print(s_node);
    Serial.println(" inside Json");
  } else
  {
    Serial.println("I don't know who I am !!!!");
  }

  //////////////////////////////////////
  /// OTHER DEVICES ARE YOUR FRIENDS ///
  //////////////////////////////////////
  Serial.println();
  Serial.println();
  Serial.println("////////////////////////////////");
  Serial.println();

  Serial.println();
  Serial.println("Master is: ");
  for (int i = 0; i < 6; i++)
  {
    Serial.print(MAC_master[i]);
    Serial.print(":");
  }
  Serial.println();
  Serial.println();
  Serial.println("////////////////////////////////");
  Serial.println();

  Serial.println("Repeaters are: ");
  Serial.println("--");
  for (int j = 0; j < nbrOfRepeaters; j++)
  {
    Serial.printf("Device n°: %i\r\n", j);
    for (int i = 0; i < 6; i++)
    {
      Serial.print(MAC_RepeatersArray[j][i]);
      Serial.print(":");
    }
    Serial.println();
  }

  Serial.println();
  Serial.println("////////////////////////////////");
  Serial.println();

  Serial.println("Counters are: ");
  Serial.println("--");
  for (int j = 0; j < nbrOfCounters; j++)
  {
    Serial.printf("Device n°: %i\r\n", j);

    for (int i = 0; i < 6; i++)
    {
      Serial.print(MAC_CountersArray[j][i]);
      Serial.print(":");
    }
    Serial.println();
    Serial.println();
  }
  Serial.println();
  Serial.println();

#ifdef COUNTER
  tft.init();
  tft.setRotation(1);
  tft.fillScreen(TFT_BLACK);


  //INPUT
  pinMode(btnUp, INPUT_PULLUP);
  pinMode(btnDown, INPUT_PULLUP);

  attachInterrupt(btnUp, fonction_ISR_UP,   FALLING);
  attachInterrupt(btnDown, fonction_ISR_DOWN,   FALLING);

  Serial.begin(115200);

  //////////////////////////////////////////////////////////////
  // ESP-NOW

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);

  // Register peer

  peerInfo.channel = 0;
  peerInfo.encrypt = false;

  // We add every MAC Address from json except Counters (for now, maybe could be interesting later) as peer

  // MASTER
  memcpy(peerInfo.peer_addr, MAC_master, 6);
  if (esp_now_add_peer(&peerInfo) != ESP_OK) {
    Serial.println("Failed to add Master");
    return;
  }
  //REPEATERS
  for (int m = 0; m < nbrOfRepeaters; m++)
  {
    memcpy(peerInfo.peer_addr, MAC_RepeatersArray[m], 6);
    // Add peer
    if (esp_now_add_peer(&peerInfo) != ESP_OK) {
      Serial.println("Failed to add repeater peer");
      return;
    }

  }

  // Register for a callback function that will be called when data is received
  esp_now_register_recv_cb(OnDataRecv);

  updateDisplay();
#endif
}




void loop()
{
#ifdef COUNTER

  //////////////////////////////////////////////////////
  ////////           BUTTONS                   /////////
  //////////////////////////////////////////////////////
  /*
    if (digitalRead(btnUp) == 1 && UpSwitchReleased == true)
    {
    Serial.println("UP");
    UpSwitchReleased = false;
    in++;
    updateDisplay();
    //delay(10);
    }

    else if (digitalRead(btnUp) == 0 && UpSwitchReleased == false)
    {
    UpSwitchReleased = true;
    //delay(10);
    }

    if (digitalRead(btnDown) == 0 && DownSwitchReleased == true)
    {
    Serial.println("DOWN");
    DownSwitchReleased = false;
    out++;
    updateDisplay();
    }

    else if (digitalRead(btnDown) == 1 && DownSwitchReleased == false)
    {
    DownSwitchReleased = true;
    }
  */

  if (millis() > lastSent + delta && hasBeenReceived == true && retry < 8)
  {
    sendData();
    // On attends une réponse
    hasBeenReceived = false;
    lastSent = millis();
    updateDisplay();
  }




  //////////////////////////////////////////////////////////
  ////////////CONNECTIVITY CHECK
  /////////////////////////////////////////////////////////

  // check connection state

  if (millis() > lastCheck + deltaCheck)
  {
    checkConnections();
    lastCheck = millis();
    updateDisplay();
  }

  /*
    if (millis()>lastUpdateReceived+ deltaUpdate)
    {
    stateConnected = false;
    } else{
    stateConnected = true;
    }

    // and update screen when state has change

    if (stateConnected == true && lastStateConnected == false)
    {
    updateDisplay();
    lastStateConnected = true;
    } else if (stateConnected == false && lastStateConnected == true)
    {
    updateDisplay();
    lastStateConnected = false;
    }


    if (incomingT != lastIncomingT)
    {
    lastIncomingT = incomingT;
    updateDisplay();
    }
  */
  if (millis() > lastUpdate + 200)
  {
    updateDisplay();
    lastUpdate = millis();
  }
  delay(10);
#endif
}

#ifdef COUNTER
//////////////////////////////////////////////////////
////////           DISPLAY                   /////////
//////////////////////////////////////////////////////

void updateDisplay()
{
  tft.fillScreen(TFT_BLACK);
  tft.setTextColor(TFT_WHITE, TFT_BLACK);
  tft.setCursor(10, 20, 4);
  tft.print("OUT [");
  tft.print(out);
  tft.print("] ");
  tft.setTextColor(TFT_RED, TFT_BLACK);
  tft.print("- ");
  tft.setTextColor(TFT_WHITE, TFT_BLACK);
  tft.print("IN [");
  tft.print(in);
  tft.print("]");
  tft.setCursor(10, 40, 4);
  tft.setTextColor(TFT_RED, TFT_BLACK);
  tft.print("-");
  tft.setCursor(10, 60, 4);
  tft.setTextColor(TFT_GREEN, TFT_BLACK);
  tft.print("Sur site: ");
  tft.print(total);
  tft.setTextColor(TFT_RED, TFT_BLACK);
  tft.setCursor(10, 80, 4);
  tft.print("-");
  tft.setTextColor(TFT_RED, TFT_BLACK);
  tft.setCursor(10, 100, 4);
  // si les messages ne passent pas, on alerte
  if (retry >= 8)
  {
    tft.print("PB DE CONNEXION");
  } else {
    if (masterConnected) {
      tft.print("M");
    } else {
      tft.print("X");
    }
    tft.print("--- ");
    tft.print(howManyRepeatersConnected);
    tft.print("/");
    tft.print(nbrOfRepeaters);
  }

}
#endif
