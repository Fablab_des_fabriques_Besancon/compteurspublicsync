/*Si problème MD5 Flash:  esptool.py --port /dev/ttyUSB0 write_flash_status --non-volatile 0

   COUNTER Select the development board ESP32 Dev Module, select Disable in the PSRAM option, select 4MB in the Flash Size option
   MASTER select M5Stack-Core-ESP32
   Upload via ESP32 data upload tool

*/
/* Role 1: COUNTER, Role 2: REPEATER, Role 3: MASTER */

#include "FS.h"
#include "SPIFFS.h"
#include <ArduinoJson.h>

#include "WiFi.h"
#include <esp_now.h>

//#define COUNTER
//#define REPEATER
#define MASTER

////////////////////////////////////////////////////////

#ifdef MASTER
#include <Preferences.h>
#include <M5Stack.h>

Preferences preferences;

#endif

// Set TRUE to format SPIFFS
#define FORMAT_SPIFFS_IF_FAILED false

// Max nbr of devices handled by the entire system
#define NBR_DEVICES 20

// JSON  where we store MACS and Roles. Upload via ESP32 data upload tool
String fileName = "config.json";
DynamicJsonDocument doc(1800);
JsonArray arr; // array extracted from json
int count; //array size

////////////////////////////////////////////////
// MAC Storage

// Array to store everyBodys MAC.
uint8_t MAC_arrays[NBR_DEVICES][6];

// MAC Address of master (only 1)
uint8_t MAC_master[6];

// Array to store repeaters MAC.
uint8_t MAC_RepeatersArray[NBR_DEVICES][6];
int nbrOfRepeaters = 0;

// Array to store Counters MAC.
uint8_t MAC_CountersArray[NBR_DEVICES][6];
int nbrOfCounters = 0;

//ME
uint8_t MAC_array[6];
int my_device = -1;
int role = 0;

////////////////////////////////////////////////

#ifdef MASTER

//////////////////////////////////////////////////////////////////////////////
//ESP-NOW related
///////////////////////////////////////////////////////////////////////////////

String success;

//DISPLAYED VARS
int in; // how many IN
int out; // how many OUT
int total; // not used when sending, set to -1

// SENDED VARS
int s_node = -1;
int s_in; // how many IN
int s_out; // how many OUT
int s_total; // not used when sending, set to -1

// Define variables to store incoming readings
int r_node;
int r_in;
int r_out;
int r_total;
String r_tree;

int lastSent = 0;
int delta = 5000;

/////////////////////////////////////////////////////////////////////
//ESP-NOW related
/////////////////////////////////////////////////////////////////////

//Structure example to send data
//Must match the receiver structure
typedef struct struct_message {
  int node; // which node has sent the message (in json order)
  int in; //how many IN
  int out; // how many OUT
  int total; //total in da place
  String tree; // we store the path followed by the message
} struct_message;

// Create a struct_message to hold sended coordinates
struct_message myCount;

// Callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
  if (status == 0) {
    success = "Delivery Success :)";
  }
  else {
    success = "Delivery Fail :(";
    //Si Compteur rate, on essaie les repeteurs
  }
}

// Callback when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  //memcpy(&receivedCoordinates, incomingData, sizeof(receivedCoordinates));
  // essai avec memmove pour éviter les débordemments

  memmove(&myCount, incomingData, sizeof(myCount));

  Serial.print("Received data from device n°");
  Serial.println(myCount.node);
  Serial.print("r_in");
  Serial.println(myCount.in);
  Serial.print("r_out");
  Serial.println(myCount.out);
  Serial.print("r_total");
  Serial.println(myCount.total);
  Serial.print("r_tree: ");
  Serial.println(myCount.tree);

  r_node = myCount.node;
  r_in = myCount.in;
  r_out = myCount.out;
  r_total = myCount.total;
  r_tree = myCount.tree;

  // -1 means direct ping
  if (r_node != -1)
  {
    // On met à jour l'écran
    in += r_in;
    out += r_out;
    total += (r_in - r_out);

    // On renvoie un message identique pour confirmer la réception, mais avec le total mis à jour,
    // directement au compteur s'il n'est pas passé par un répéteur
    if (r_tree.length() == 0)
    {
      sendAnswer(MAC_arrays[r_node],r_node, r_in, r_out, total, r_tree);
    } else {
      Serial.print("r_tree before");
      Serial.println(r_tree);
      // On renvoie au répéteur en choisissant le dernier noeud indiqué dans la chaîne tree
      char myTree[r_tree.length()+1];
      r_tree.toCharArray(myTree, r_tree.length()+1);
      char *ptr = NULL;
      byte index = 0;
      ptr = strtok(myTree, ",");  // delimiter
      int storedTree[20];
      while (ptr != NULL)
      {
        storedTree[index] = atoi(ptr);
        index++;
        ptr = strtok(NULL, ",");
      }
      // On supprime le dernier noeud et on renvoie la chaine
      String updatedTree;
      for (int i =0; i<index-1; i++)
      {
        updatedTree += String(storedTree[i]);
        updatedTree +=",";
      }
      Serial.print("r_tree after");
      Serial.println(updatedTree);
      Serial.println();
      Serial.print("on envoie au numero ");
      Serial.println(storedTree[index-1]);
      sendAnswer(MAC_arrays[storedTree[index-1]],r_node, r_in, r_out, total, updatedTree);
    }
  }
}

void sendAnswer(uint8_t _mac[6],int _node, int _in, int _out, int _total, String _tree)
{
  struct_message myAnswer;

  myAnswer.node = _node;
  myAnswer.in = _in;
  myAnswer.out = _out;
  myAnswer.total = _total;
  myAnswer.tree = _tree;

  esp_err_t result = esp_now_send(_mac, (uint8_t *) &myAnswer, sizeof(myAnswer));
  if (result == ESP_OK) {
    Serial.print("Answer sent with success to device n° ");
    Serial.println(_node);

  }
  else {
    Serial.print("Answer error device n° ");
    Serial.println(_node);
  }
}
/*
  void sendToOthers()
  {
  //Send my text to other devices
  struct_message myTotal;
  myTotal.c = total;
  for (int m = 0; m < nbrOfDevices; m++)
  {
    esp_err_t result = esp_now_send(broadcastAddresses[m], (uint8_t *) &myTotal, sizeof(myTotal));
    if (result == ESP_OK) {
      Serial.print("Sent with success to MAC ");
      Serial.println(m);

    }
    else {
      Serial.print("Error sending the data to MAC ");
      Serial.println(m);
    }
  }
  }
*/
esp_now_peer_info_t peerInfo;

#endif

// Récuparation de la liste des adresses MAC dans un fichier Json stocké dans la SPIFF
void listDir(fs::FS &fs, const char * dirname, uint8_t levels)
{
  Serial.println();
  Serial.printf("Listing directory: %s\r\n", dirname);

  File root = fs.open(dirname);
  // On parcours les dossiers et les fichiers à l'intérieur
  if (!root)
  {
    Serial.println("- failed to open directory");
    return;
  }
  if (!root.isDirectory())
  {
    Serial.println(" - not a directory");
    return;
  }

  File file = root.openNextFile();

  while (file)
  {
    if (file.isDirectory())
    {
      Serial.print("  DIR : ");
      Serial.println(file.name());
      if (levels)
      {
        listDir(fs, file.name(), levels - 1);
      }
    }
    else
    {
      Serial.print("FILE: ");
      Serial.print(file.name());
      Serial.print("\tSIZE: ");
      Serial.println(file.size());

      String str1 = String(file.name()); //convert char to String
      //
      // On cherche un fichier précis, config.json
      if (str1 == fileName)
      {
        Serial.println("Found config.json");

        auto error = deserializeJson(doc, file);
        if (error) {
          Serial.println("Failed to parse the file");
          // return false;
        }
        else
        {
          Serial.println("Success to parse the file");
          // Get a reference to the root array
          arr = doc.as<JsonArray>();

          // Get the number of elements in the array
          count = arr.size();
          int iterator = 0;

          // Walk the JsonArray efficiently
          for (JsonObject repo : arr)
          {
            Serial.println();
            Serial.println();
            Serial.printf("Device n°: %i\r\n", iterator);
            Serial.println("---");
            Serial.print("Role: ");
            int temp_role = repo["role"];
            Serial.println(temp_role);

            for (int i = 0; i < 6; i++)
            {
              const char* temp_mac = repo["mac"][i];
              char temp[5]; // Temporary space for the conversion string
              // Copy 4 characters into the temporary string
              temp[0] = temp_mac[0];
              temp[1] = temp_mac[1];
              temp[2] = temp_mac[2];
              temp[3] = temp_mac[3];
              // and terminate the string properly
              temp[4] = 0;
              // Convert the string using base 16
              uint8_t val = strtol(temp, NULL, 16);
              MAC_arrays[iterator][i] = val;
              Serial.print(val);
              Serial.print(":");

              // Storing it into the right Array
              /* Role 1: COUNTER, Role 2: REPEATER, Role 3: MASTER */
              switch (temp_role)
              {
                //COUNTER
                case 1:
                  MAC_CountersArray[nbrOfCounters][i] = val;
                  break;

                //REPEATER
                case 2:
                  MAC_RepeatersArray[nbrOfRepeaters][i] = val;
                  break;

                //MASTER
                case 3:
                  MAC_master[i] = val;
                  break;
              }

            }
            switch (temp_role)
            {
              //COUNTER
              case 1:
                nbrOfCounters++;
                break;

              //REPEATER
              case 2:
                nbrOfRepeaters++;
                break;

            }

            iterator++;
          }
          Serial.println();
          Serial.println("---");

          return;
        }

      }
    }
    file = root.openNextFile();
  }
}



void setup()
{

  Serial.begin(115200);

  if (!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED))
  {
    Serial.println("SPIFFS Mount Failed");
    return;
  }
  listDir(SPIFFS, "/", 0);

  /////////////////////////////////
  /// IDENTIFICATION OF DEVICES ///
  /////////////////////////////////

  //MY MAC
  Serial.println();
  Serial.print("My Mac is: ");
  WiFi.macAddress(MAC_array);
  for (int i = 0; i < sizeof(MAC_array); ++i)
  {
    //sprintf(MAC_char,"%s%02x:",MAC_char,MAC_array[i]);
    Serial.print(MAC_array[i]);
    Serial.print(":");
  }

  Serial.println();
  Serial.println();

  //COMPARAISON
  for (int m = 0; m < NBR_DEVICES; m++)
  {
    int score = 0;
    for (int n = 0; n < 6; n++)
    {
      if (MAC_array[n] == MAC_arrays[m][n])score++;
    }
    if (score == 6) my_device = m;
  }
  if (my_device != -1)
  {
    Serial.print("My Device is n° ");
    Serial.print(my_device);
    Serial.println("inside Json");
  } else
  {
    Serial.println("I don't know who I am !!!!");
  }

  //////////////////////////////////////
  /// OTHER DEVICES ARE YOUR FRIENDS ///
  //////////////////////////////////////
  Serial.println();
  Serial.println();
  Serial.println("////////////////////////////////");
  Serial.println();

  Serial.println();
  Serial.println("Master is: ");
  for (int i = 0; i < 6; i++)
  {
    Serial.print(MAC_master[i]);
    Serial.print(":");
  }
  Serial.println();
  Serial.println();
  Serial.println("////////////////////////////////");
  Serial.println();

  Serial.println("Repeaters are: ");
  Serial.println("--");
  for (int j = 0; j < nbrOfRepeaters; j++)
  {
    Serial.printf("Device n°: %i\r\n", j);
    for (int i = 0; i < 6; i++)
    {
      Serial.print(MAC_RepeatersArray[j][i]);
      Serial.print(":");
    }
    Serial.println();
  }

  Serial.println();
  Serial.println("////////////////////////////////");
  Serial.println();

  Serial.println("Counters are: ");
  Serial.println("--");
  for (int j = 0; j < nbrOfCounters; j++)
  {
    Serial.printf("Device n°: %i\r\n", j);

    for (int i = 0; i < 6; i++)
    {
      Serial.print(MAC_CountersArray[j][i]);
      Serial.print(":");
    }
    Serial.println();
    Serial.println();
  }
  Serial.println();
  Serial.println();

#ifdef MASTER
  M5.begin();
  M5.Lcd.fillScreen(BLACK);

  M5.Lcd.setTextColor(GREEN);
  Serial.begin(115200);

  //////////////////////////////////////////////////////////////
  // ESP-NOW

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  //esp_now_register_send_cb(OnDataSent);

  // Register peer

  peerInfo.channel = 0;
  peerInfo.encrypt = false;

  // We add every MAC Address from json except Counters (for now, maybe could be interesting later) as peer

  // MASTER
  /*
    memcpy(peerInfo.peer_addr, MAC_master, 6);
    if (esp_now_add_peer(&peerInfo) != ESP_OK) {
        Serial.println("Failed to add Master");
        return;
    }
  */
  //REPEATERS
  for (int m = 0; m < nbrOfRepeaters; m++)
  {
    memcpy(peerInfo.peer_addr, MAC_RepeatersArray[m], 6);
    // Add peer
    if (esp_now_add_peer(&peerInfo) != ESP_OK) {
      Serial.println("Failed to add repeater peer");
      return;
    }
  }

  //COUNTERS
  for (int m = 0; m < nbrOfCounters; m++)
  {
    memcpy(peerInfo.peer_addr, MAC_CountersArray[m], 6);
    // Add peer
    if (esp_now_add_peer(&peerInfo) != ESP_OK) {
      Serial.println("Failed to add counter peer");
      return;
    }
  }


  // Register for a callback function that will be called when data is received
  esp_now_register_recv_cb(OnDataRecv);

  // The master remembers his last total when shutdown or reboot
  preferences.begin("my-app", false);
  int storedTotal = preferences.getUInt("total", 0);
  total = storedTotal;
  int storedIN = preferences.getUInt("in", 0);
  in = storedIN;
  int storedOUT = preferences.getUInt("out", 0);
  out = storedOUT;

#endif
}

void loop()
{
#ifdef MASTER

  // Update screen
  if (millis() > lastSent + delta)
  {
    //sendToOthers();
    lastSent = millis();
    M5.Lcd.fillScreen(BLACK);

    M5.Lcd.setTextColor(WHITE);
    M5.Lcd.setTextSize(2);
    M5.Lcd.setCursor(10, 10);
    M5.Lcd.printf("IN: ");
    M5.Lcd.println(in);

    M5.Lcd.setCursor(10, 30);
    M5.Lcd.printf("OUT: ");
    M5.Lcd.print(out);

    M5.Lcd.setCursor(10, 50);
    M5.Lcd.printf("TOTAL SUR SITE: ");
    M5.Lcd.print(total);

    M5.Lcd.setCursor(10, 100);
    M5.Lcd.setTextColor(YELLOW);
    M5.Lcd.setTextSize(2);
    M5.Lcd.println("Eteindre: double clic sur");
    M5.Lcd.setCursor(10, 120);
    M5.Lcd.println("bouton lateral rouge.");
    
    M5.Lcd.setCursor(10, 150);
    M5.Lcd.setTextColor(YELLOW);
    M5.Lcd.setTextSize(2);
    M5.Lcd.println("Bouton 1 pendant 1s");
    M5.Lcd.setCursor(10, 170);
    M5.Lcd.println("=> remise a 0");
    
    backupTotal();
  }

  // Listen to button > to reset zero
  M5.update();

  if (M5.BtnA.wasReleasefor(1000)) {
    M5.Lcd.clear(BLACK);  // Clear the screen and set white to the background color.
    total = 0;
    in = 0;
    out = 0;
    preferences.putUInt("total", 0);
    preferences.putUInt("in", 0);
    preferences.putUInt("out", 0);
  }

}


void backupTotal()
{
  preferences.putUInt("total", total);
  preferences.putUInt("in", in);
  preferences.putUInt("out", out);
}
#endif
