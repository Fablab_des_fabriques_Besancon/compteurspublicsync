# Compteurs de public synchronisés
## Système de comptage distribué, sans fil pour les manifestations à jauge limitée

Ce système de boitiers synchronisés avec un module central permet de connaître en temps réél le nombre de personnes présentes dans un espace à jauge limitée, et comportant plusieurs entrées et sorties. 
À chaque point d'entrée du public sur un site, une personne décompte à l'aide des deux boutons d'un boitier les personnes entrant et les personnes sortant. Le module central calcule en permanence le total à partir des informations récupérées et envoie le total sur chaque boitier.

Les informations son transmises sans fil sur une distance d'environ 100 mètres en extérieur, soit la possibilité d'utiliser le système sur un site de 200m de diamètre, en posant le module central au milieu.

Très simple d'utilisation, le système garde en mémoire le total en cas de coupure intempestive ou de redémarrage.

![](pics/IMG_6639.jpg)
![](pics/compteurs_principe.png)

### Hardware

Le module central utilise un **M5 Stack** [1] basé sur le microcontrôleur ESP32: celui-ci offre la possibilité d'exploiter un écran confortable, des boutons intégrés, une petite batterie (même si ce nœud central a vocation à être branché au secteur). La carte SD que l'on peut y adjoindre permettra à terme de logger les informations de fréquentation, voire d'afficher des courbes en temps réel sur l'écran.

Les compteurs proprement dits sont basés sur les cartes **TTGO ESP32 T-Display** [2]. L'écran OLED couleur intégré et la gestion de la batterie font qu'il n'y a presque rien à ajouter pour finaliser le projet. Les deux boutons présents sur la carte auraient presque pu suffire, mais trop petits, ils ont été remplacés par des boutons du fabriquant *Honeyone* [3].

Une batterie 2000 mAh assure l'autonomie des clients. Un switch [4] permet de couper l'alimentation.

Le boitier a été modélisé à l'aide du logiciel Freecad et imprimé en PLA.

<img src="pics/IMG_6633.jpg" width="266" height="400" /> <img src="pics/IMG_6641.jpg" width="266" height="400" />   
  
### Software

Nous utilisons le protocole de communication **ESP-Now** [5] développé par Espressif pour faire communiquer clients et nœud central. Idéal pour ce genre d'application, il permet en sus de s'assurer de la bonne transmission des informations (et à défaut de les garder en mémoire pour une seconde tentative). Seul inconvénient, il nécessite de rentrer en dur les adresses MAC des modules utilisés pour les appairer.

La gestion de l'affichage sur le M5 tire parti de la bibliothèque M5Stack disponible sur Github [6]. Sur les modules TTGO, nous utilisons TTGO-T-Display [7]. Pour stocker les informations dans l'EEPROM et les rendre résistantes aux redémarrages, nous utilisons <Preferences.h> [8].

### To do

- Affichage du total sur la journée
- Log des données sur carte SD, affichage sous forme de courbes sur l'écran     
- Ajout d'autres clients    
- Ajout d'autres master pour augmenter la portée


[1] https://m5stack.com/   
[2] http://www.lilygo.cn/prod_view.aspx?TypeId=50044&Id=1126&FId=t3:50044:3   
[3] http://www.honyone.com/en/products/list_42_1.html   
[4] Modèle 09-10201-02 https://www.farnell.com/datasheets/1683364.pdf   
[5] https://www.espressif.com/en/products/software/esp-now/overview   
[6] https://github.com/m5stack/M5Stack   
[7] https://github.com/Xinyuan-LilyGO/TTGO-T-Display   
[8] https://randomnerdtutorials.com/esp32-save-data-permanently-preferences/



